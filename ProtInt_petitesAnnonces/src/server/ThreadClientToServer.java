package server;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Observable;
import java.util.Observer;

import ad.Ad;
import protocol.Protocol;
import user.User;

public class ThreadClientToServer extends Thread implements Observer{
	private Socket client;
	private DataOutputStream dos;
	private DataInputStream dis;
	
	public ThreadClientToServer(Socket client){
		super();
		this.client = client;
		try{
			this.dos = new DataOutputStream(client.getOutputStream());
			this.dis = new DataInputStream(client.getInputStream());
		}catch(Exception e){
			System.exit(0);
		}
	}
	public void run(){
		User user = null;
		ServerInstance server = ServerInstance.getInstance();
		ServerObservable.getInstance().addObserver(this);
		Protocol protocol = new Protocol();
		try{
			// Get the udp port of client
			String read = dis.readUTF();
			String[] clientUDP;
			clientUDP = Protocol.returnMessages(read);
			if(server.getDebug()){
				System.out.println(
						"["+client.getInetAddress().getHostAddress()+"] ["
						+client.getPort()+"] "
						+Protocol.displayableMessage(read));
			}
			
			if( (clientUDP.length != 2) ||
				!(clientUDP[0].equals("USR")) ||
				!(clientUDP[1].subSequence(0, 4).equals("PORT"))){
				dos.writeUTF(protocol.messageRefusedRequest());
				dos.flush();
				client.close();
			}else{
				user = new User(client.getInetAddress().getHostAddress(),
									client.getPort(),
									Integer.parseInt(clientUDP[1].substring(5, clientUDP[1].length())));
				server.addUser(user);
				int[] adIds = server.getAdIds();
				for(int i=0; i<adIds.length; i++){
					Ad ad = server.getAd(adIds[i]);
					dos.writeUTF("AD"+Protocol.MESSAGE_SEPARATION+"SEND"+Protocol.MESSAGE_SEPARATION
									+"ID "+ad.getId()+Protocol.MESSAGE_SEPARATION
									+"IPV4 "+ad.getIp()+Protocol.MESSAGE_SEPARATION
									+"PORT "+ad.getPort()+Protocol.MESSAGE_SEPARATION
									+"MSG "+ad.getMessage()+Protocol.MESSAGE_SEPARATION);
					dos.flush();
				}
				mainLoop: // define to exit
				while(true){
					String[] message;
					read = dis.readUTF();
					if(server.getDebug()){
						System.out.println(
								"["+client.getInetAddress().getHostAddress()+"] ["
								+client.getPort()+"] "
								+Protocol.displayableMessage(read));
					}
					message = Protocol.returnMessages(read);
					// br read if exits one byte in the message
					switch(message[0]){
					case "AD":
						switch(message[1]){
						case "DEL":
							if(message[2].substring(0, 2).equals("ID")){
								try{
									int id = Integer.parseInt(message[2].substring(3, message[2].length()));
									if( (server.getAd(id).getIp().equals(user.getIp())) &&
										(server.getAd(id).getPort() == user.getPortUdp())){
										server.removeAd(id);
									}else{
										dos.writeUTF(protocol.messageRefusedRequest());
										dos.flush();
									}
								}catch(Exception e){
									dos.writeUTF(protocol.messageRefusedRequest());
									dos.flush();
								}
							}else{
								dos.writeUTF(protocol.messageBadRequest());
								dos.flush();
							}
							break;
						case "ADD":
							if(message[2].substring(0, 3).equals("MSG")){
								Ad ad = new Ad(user.getIp(),
											user.getPortUdp(),
											message[2].substring(4, message[2].length()));
								server.addAd(ad);
							}else{
								dos.writeUTF(protocol.messageBadRequest());
								dos.flush();
							}
							break;
						default:
							break;
						}
						break;
					case "DISCONNECT":
						server.removeUser(user);
						break mainLoop;
					default:
						dos.writeUTF(protocol.messageBadRequest());
						dos.flush();
						break;
					}
					Thread.sleep(100);
				}
			}
		}catch(Exception e){
			// Connection lost, not print exception
			if(user != null)
				server.removeUser(user);
		}
		try {
			client.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		ServerObservable.getInstance().deleteObserver(this);
	}
	@Override
	public void update(Observable o, Object arg) {
		if(o instanceof ServerObservable){
			if(arg instanceof String){
				try {
					dos.writeUTF((String)arg);
					dos.flush();
				} catch (IOException e) {
					// Connection lost, not print exception
				}
			}
		}
	}
}
