package server;

import java.net.ServerSocket;
import java.net.Socket;

public class ThreadServer extends Thread{
	private ServerSocket server;
	public ThreadServer(ServerSocket server){
		this.server = server;
	}
	public void run(){
		while(true){
			try {
				Socket client = server.accept();
				ThreadClientToServer t = new ThreadClientToServer(client);
				t.start();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
