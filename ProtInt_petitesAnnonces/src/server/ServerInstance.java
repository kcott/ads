package server;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import ad.Ad;
import protocol.Protocol;
import user.User;

public class ServerInstance {
	/*
	 * Singleton pattern
	 */
	private static ServerInstance s = null;
	private ServerSocket serverSocket = null;
	private ServerInstance(){
		idAd = 1;
		ads = new HashMap<Integer,Ad>();
		users = new LinkedList<User>();
		debug = false;
		try{
			serverSocket = new ServerSocket(Protocol.SERVER_PORT);
			System.out.println(
					"["+InetAddress.getLocalHost().getHostAddress()+":"+
					Protocol.SERVER_PORT+"]");
		}catch(Exception e){
			e.printStackTrace();
			System.exit(1);
		}
	}
	public static ServerInstance getInstance(){
		if(s == null)
			s = new ServerInstance();
		return s;
	}
	
	/*
	 * Real class
	 */
	// ads list
	private boolean debug;
	public int idAd;
	private Map<Integer,Ad> ads;
	private List<User> users;
	public boolean getDebug(){
		return debug;
	}
	public synchronized void setDebug(boolean debug){
		this.debug = debug;
	}
	public synchronized void addAd(Ad ad){
		ad.setId(idAd);
		ads.put(idAd, ad);
		idAd++;
		ServerObservable.getInstance().updateAd(
				"AD"+Protocol.MESSAGE_SEPARATION+"SEND"+Protocol.MESSAGE_SEPARATION+
				"ID "+ad.getId()+Protocol.MESSAGE_SEPARATION+
				"IPv4 "+ad.getIp()+Protocol.MESSAGE_SEPARATION+
				"PORT "+ad.getPort()+Protocol.MESSAGE_SEPARATION+
				"MSG "+ad.getMessage()+Protocol.MESSAGE_SEPARATION);
		notifyAll();
	}
	public synchronized void removeAd(int id){
		ads.remove(id);
		ServerObservable.getInstance().updateAd(
				"AD"+Protocol.MESSAGE_SEPARATION+"DEL"+Protocol.MESSAGE_SEPARATION+
				"ID "+id+Protocol.MESSAGE_SEPARATION);
		notifyAll();
	}
	public synchronized Ad getAd(int id){
		return ads.get(id);
	}
	public synchronized int[] getAdIds(){
		int[] result = new int[ads.size()];
		int i=0;
		for(Map.Entry<Integer, Ad> entry:ads.entrySet()){
			result[i++] = entry.getKey();
		}
		return result;
	}
	public synchronized void addUser(User user){
		if(user != null){
			users.add(user);
		}
		notifyAll();
	}
	public synchronized void removeUser(User user){
		if(user != null){
			LinkedList<Integer> indexes = new LinkedList<Integer>();
			for(Map.Entry<Integer, Ad> entry:ads.entrySet()){
				if( (entry.getValue().getIp().equals(user.getIp())) &&
					(entry.getValue().getPort() == user.getPortUdp())){
					indexes.add(entry.getKey());
				}
			}
			for(int index:indexes){
				removeAd(index);
			}
			users.remove(user);
		}
		notifyAll();
	}
	public void start(){
		Scanner sc = new Scanner(System.in);
		ThreadServer server = new ThreadServer(serverSocket);
		server.start();
		loopServer:
		while(true){
			System.out.print("> ");
			switch(sc.nextLine()){
			case ActionOnServer.HELP:
			case "?":
				displayHelp();
				break;
			case ActionOnServer.EXIT:
				break loopServer;
			case ActionOnServer.DEBUG:
				setDebug(!getDebug());
				break;
			case ActionOnServer.ADDRESS:
				try {
					System.out.println(
							"["+InetAddress.getLocalHost().getHostAddress()+":"+
							Protocol.SERVER_PORT+"]");
				} catch (UnknownHostException e) {
					e.printStackTrace();
				}
				break;
			case ActionOnServer.LIST_ADS:
				for(Map.Entry<Integer, Ad> entry:ads.entrySet()){
					System.out.print("["+entry.getKey()+"]");
					System.out.print("["+entry.getValue().getIp()+":"+entry.getValue().getPort()+"]");
					System.out.println(" "+entry.getValue().getMessage());
				}
				break;
			case ActionOnServer.LIST_USERS:
				System.out.println("[ip_address] [tcp_port] [udp_port]");
				for(User user:users){
					System.out.println("["+user.getIp()+"] ["+user.getPort()+"] ["+user.getPortUdp()+"]");
				}
				break;
			default:
				System.out.println("Error action, see \"help\"");
			}
		}
		sc.close();
		System.exit(0);
	}
	public void displayHelp(){
		System.out.println("Action on server:");
		System.out.println("\t"+ActionOnServer.EXIT+"\texit the application");
		System.out.println("\t"+ActionOnServer.DEBUG+"\tactivate/desactivation debug mode");
		System.out.println("\t"+ActionOnServer.ADDRESS+"\taddress of the server");
		System.out.println("\t"+ActionOnServer.LIST_ADS+"\tlist ads");
		System.out.println("\t"+ActionOnServer.LIST_USERS+"\tlist users");
	}
}
