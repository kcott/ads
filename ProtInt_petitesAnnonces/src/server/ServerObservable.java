package server;
import java.util.Observable;

/**
 * Observer who transmit new data to clients
 */
public class ServerObservable extends Observable{
	/*
	 * Singleton pattern
	 */
	private ServerObservable(){}
	private static ServerObservable observer = null;
	public static ServerObservable getInstance(){
		if(observer == null)
			observer = new ServerObservable();
		return observer;
	}
	/*
	 * Real class
	 */
	public synchronized void updateAd(String message){
		this.setChanged();
		this.notifyObservers(message);
		this.clearChanged();
	}
}
