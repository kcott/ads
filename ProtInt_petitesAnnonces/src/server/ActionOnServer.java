package server;

public class ActionOnServer {
	public static final String HELP = "help";
	public static final String EXIT = "exit";
	public static final String ADDRESS = "address";
	public static final String LIST_ADS = "ads";
	public static final String LIST_USERS = "users";
	public static final String DEBUG = "debug";
}
