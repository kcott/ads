package protocol;

public class Protocol {	
	public static final int SERVER_PORT = 1027;
	public static final String MESSAGE_SEPARATION = "\r\n";
	public static String[] cleanMessages(String[] messages){
		for(int i=0; i<messages.length; i++)
			messages[i] = messages[i].trim();
		return messages;
	}
	public static String[] returnMessages(String message){
		return cleanMessages(message.split(MESSAGE_SEPARATION));
	}
	public static String displayableMessage(String message){
		String readDisplayable = message.replace(Protocol.MESSAGE_SEPARATION, "$(SEP)"); 
		return readDisplayable+"$(END)";
	}
	
	public String messageBadRequest(){
		return "SYS"+MESSAGE_SEPARATION+"ERROR 001"+MESSAGE_SEPARATION;
	}
	public String messageRefusedRequest(){
		return "SYS"+MESSAGE_SEPARATION+"ERROR 002"+MESSAGE_SEPARATION;
	}
}
