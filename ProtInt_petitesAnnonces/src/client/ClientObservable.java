package client;
import java.util.Observable;

public class ClientObservable extends Observable{
	/*
	 * Singleton pattern
	 */
	private ClientObservable(){}
	private static ClientObservable observer = null;
	public static synchronized ClientObservable getInstance(){
		if(observer == null)
			observer = new ClientObservable();
		return observer;
	}
	/*
	 * Real class
	 */
	public synchronized void getMessage(ObjectClientObservable message){
		this.setChanged();
		this.notifyObservers(message);
		this.clearChanged();
	}
}
