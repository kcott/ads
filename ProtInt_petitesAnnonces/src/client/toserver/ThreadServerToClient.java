package client.toserver;

import java.io.DataInputStream;
import java.net.Socket;

import ad.Ad;
import client.ClientInstance;
import client.toclient.Transaction;
import protocol.Protocol;

public class ThreadServerToClient extends Thread{
	private Socket socketServer;
	public ThreadServerToClient(Socket socketServer){
		this.socketServer = socketServer;
	}
	public void run(){
		ClientInstance client = ClientInstance.getInstance();		
		try{
			DataInputStream dis = new DataInputStream(socketServer.getInputStream());
			while(true){
				if(dis.available()>=0){
					try{
						String[] message = Protocol.returnMessages(dis.readUTF());
						int indexTrait = 0;
						argsLoop:
						while(indexTrait < message.length){
							switch(message[indexTrait+0]){
							case "AD":
								switch(message[indexTrait+1]){
								case "SEND":
									Ad ad = new Ad( message[indexTrait+3].substring(5, message[indexTrait+3].length()),
											 		Integer.parseInt(message[indexTrait+4].substring(5, message[indexTrait+4].length())),
											 		message[indexTrait+5].substring(4, message[indexTrait+5].length()));
									client.addAd(Integer.parseInt(message[indexTrait+2].substring(3, message[indexTrait+2].length())),
												 ad);
									indexTrait += 6;
									break;
								case "DEL":
									int id = Integer.parseInt(message[indexTrait+2].substring(3, message[indexTrait+2].length()));
									client.removeAd(id);
									client.declinedTransaction(id);
									Transaction[] ts = client.getTransactionsToConclude();
									for(int i=0; i<ts.length; i++){
										if(ts[i].getAd() == id)
											client.declineTransaction(ts[i]);
									}
									indexTrait += 3;
									break;
								}
								break;
							default:
								break argsLoop;
							}
						}
					}catch(NumberFormatException e){}
				}
				Thread.sleep(100);
			}
		}catch(Exception e){
			// CONNECTION SERVER LOST;
		}
		System.exit(0);
	}
}
