package client.toclient;

import client.ObjectClientObservable;

public class MessageClientObservable implements ObjectClientObservable{
	private int id;
	private String ip;
	private int udpPort;
	private String message;
	public MessageClientObservable(int id, String ip, int udpPort, String message){
		this.id = id;
		this.ip = ip;
		this.udpPort = udpPort;
		this.message = message;
	}
	public int getId(){
		return id;
	}
	public String getIp(){
		return ip;
	}
	public int getUdpPort(){
		return udpPort;
	}
	public String getMessage(){
		return message;
	}
}
