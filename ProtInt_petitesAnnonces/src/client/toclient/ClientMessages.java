package client.toclient;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.InetAddress;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import client.ClientInstance;
import client.ClientObservable;
import protocol.Protocol;

public class ClientMessages extends JPanel implements ActionListener, Observer{
	private static final long serialVersionUID = 1L;
	private JTextArea messages;
	private JTextArea message;
	private Transaction transaction;
	
	private final String persoIndicator = "[ME]";
	private final String clientIndicator = "[HE]";
	
	public ClientMessages(Transaction transaction){
		super();
		
		this.transaction = transaction;
		ClientObservable.getInstance().addObserver(this);
		
		BorderLayout layout = new BorderLayout();
		setLayout(layout);
		
		JLabel transactionIdPanel = new JLabel();
		transactionIdPanel.setText(this.transaction.getUser().getIp()
									+":"+this.transaction.getUser().getPortUdp()
									+"["+this.transaction.getAd()+"]");
		add(transactionIdPanel, BorderLayout.NORTH);
		
		messages = new JTextArea();
		messages.setEditable(false);
		JScrollPane pane = new JScrollPane(messages);
		add(pane, BorderLayout.CENTER);
		
		GridLayout grid = new GridLayout(1, 0);
		JPanel pgrid = new JPanel();
		pgrid.setLayout(grid);
		add(pgrid, BorderLayout.SOUTH);
		
		message = new JTextArea(1, 0);
		pgrid.add(message);
		
		JButton send = new JButton("send");
		send.addActionListener(this);
		pgrid.add(send);
	}
	
	@Override
	public void update(Observable o, Object arg) {
		try{
			if(o instanceof ClientObservable){
				if(arg instanceof MessageClientObservable){
					MessageClientObservable m = (MessageClientObservable) arg;
					if((m.getId() == transaction.getAd())
							&& (m.getIp().equals(InetAddress.getLocalHost().getHostAddress()))
							&& (m.getUdpPort() == ClientInstance.getInstance().getUDPPort()))
						messages.setText(messages.getText()+persoIndicator+m.getMessage()+"\n");
					else if((m.getId() == transaction.getAd())
							&& (m.getIp().equals(transaction.getUser().getIp()))
							&& (m.getUdpPort() == transaction.getUser().getPortUdp()))
						messages.setText(messages.getText()+clientIndicator+m.getMessage()+"\n");
				}
			}
		}catch(Exception e){}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		String m = message.getText();
		message.setText("");
		messages.setText(messages.getText()+persoIndicator+m+"\n");
		SendClientObservable mco = new SendClientObservable(
				transaction.getUser().getIp(),
				transaction.getUser().getPortUdp(),
				"AD"+Protocol.MESSAGE_SEPARATION+
				"ID "+transaction.getAd()+Protocol.MESSAGE_SEPARATION+
				"MSG "+m+Protocol.MESSAGE_SEPARATION);
		ClientObservable.getInstance().getMessage(mco);
	}
}
