package client.toclient;

import client.ObjectClientObservable;

public class SendClientObservable implements ObjectClientObservable{
	private String ip;
	private int udpPort;
	private String message;
	public SendClientObservable(String ip, int udpPort, String message){
		this.ip = ip;
		this.udpPort = udpPort;
		this.message = message;
	}
	public String getIp(){
		return ip;
	}
	public int getUdpPort(){
		return udpPort;
	}
	public String getMessage(){
		return message;
	}
}
