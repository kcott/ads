package client.toclient;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Observable;
import java.util.Observer;

import client.ClientInstance;
import client.ClientObservable;
import protocol.Protocol;
import user.User;

public class ThreadClientToClient extends Thread implements Observer{
	private final int SIZE_BUFFER = 1024;
	private DatagramSocket socketUDP;
	public ThreadClientToClient(DatagramSocket socketUDP){
		this.socketUDP = socketUDP;
	}
	public void run(){
		byte[] datas = new byte[SIZE_BUFFER];
		DatagramPacket packet = new DatagramPacket(datas, datas.length);
		ClientObservable.getInstance().addObserver(this);
		try {
			int id;
			while(true){
				socketUDP.receive(packet);
				String data = new String(packet.getData(), 0, packet.getLength());
				String[] message = Protocol.returnMessages(data);
				switch(message[0]){
				case "AD":
					switch(message[1]){
					case "REFUSE":
						id = Integer.parseInt(message[2].substring(3, message[2].length()));
						ClientInstance.getInstance().declinedTransaction(id);
						break;
					case "QUERY":
						id = Integer.parseInt(message[2].substring(3, message[2].length()));
						Transaction transaction = new Transaction(
								new User(packet.getAddress().getHostAddress().toString(),
										0,
										packet.getPort()),
										id);
						ClientInstance.getInstance().addTransactionToConclude(transaction);
						break;
					case "ACCEPT":
						id = Integer.parseInt(message[2].substring(3, message[2].length()));
						ClientInstance.getInstance().acceptedTransaction(id);
						break;
					default:
						id = Integer.parseInt(message[1].substring(3, message[1].length()));
						Transaction t = new Transaction(
								new User(packet.getAddress().getHostAddress().toString(),
										0,
										packet.getPort()),
										id);
						ClientInstance.getInstance().addClientTab(t);
						ClientObservable.getInstance().getMessage(new MessageClientObservable(
								id,
								packet.getAddress().getHostAddress().toString(),
								packet.getPort(),
								message[2].substring(4, message[2].length())
								));
						break;
					}
				default:
					break;
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		ClientObservable.getInstance().deleteObserver(this);
	}
	
	@Override
	public void update(Observable o, Object arg) {
		if(o instanceof ClientObservable){
			if(arg instanceof SendClientObservable){
				SendClientObservable object = (SendClientObservable) arg;
				try {
					DatagramPacket packet = new DatagramPacket(
							object.getMessage().getBytes(),
							object.getMessage().getBytes().length,
							InetAddress.getByName(object.getIp()),
							object.getUdpPort()
							);
					socketUDP.send(packet);
				} catch (UnknownHostException e) {
				} catch (IOException e) {}
			}
		}
	}
}
