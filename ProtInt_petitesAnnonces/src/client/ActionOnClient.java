package client;

public class ActionOnClient {
	public static final String HELP = "help";
	public static final String EXIT = "exit";
	public static final String LIST_ADS = "ads";
	public static final String ADD_AD = "add";
	public static final String REMOVE_AD = "remove";
	public static final String LIST_TRANSACTIONS_ASKING = "ask";
	public static final String LIST_TRANSACTIONS_TO_CONCLUDE = "answer";
	public static final String SEND_MESSAGE_TO_CLIENT = "msg";
	public static final String ASK_TRANSACTION = "want";
	public static final String ACCEPT_TRANSACTION = "accept";
	public static final String DECLINE_TRANSACTION = "refuse";
	public static final String ADDRESS = "address";
}
