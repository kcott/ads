package client;
import java.awt.BorderLayout;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import javax.swing.JFrame;
import javax.swing.JTabbedPane;

import ad.Ad;
import client.toclient.SendClientObservable;
import client.toclient.ClientMessages;
import client.toclient.MessageClientObservable;
import client.toclient.ThreadClientToClient;
import client.toclient.Transaction;
import client.toserver.ThreadServerToClient;
import protocol.Protocol;
import user.User;

public class ClientInstance extends JFrame{
	private static final long serialVersionUID = 1L;
	/*
	 * Singleton pattern
	 */
	private static ClientInstance c = null;
	private ClientInstance(){
		super();
		
		ads  = new HashMap<Integer,Ad>();
		transactionsToConclude = new ArrayList<Transaction>();
		transactionsAsking = new ArrayList<Integer>();
		messagesClient = new HashMap<Transaction, ClientMessages>();
		
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setLayout(new BorderLayout());
		tabbedClientMessages = new JTabbedPane();
		add(tabbedClientMessages);
		
		setSize(400, 300);
		setVisible(true);
	}
	public static ClientInstance getInstance(){
		if (c == null)
			c = new ClientInstance();
		return c;
	}
	
	/*
	 * Start of real class
	 */
	private Socket socketServer;
	private Thread threadServer;
	private DatagramSocket socketUDP;
	private Thread threadUDP;
	
	private DataOutputStream dos;
	private PrintWriter pw;
	private Scanner scanner;

	private Map<Integer,Ad> ads;
	private List<Transaction> transactionsToConclude;
	private List<Integer> transactionsAsking;
	
	private JTabbedPane tabbedClientMessages; 
	private Map<Transaction, ClientMessages> messagesClient;
	
	public void setSockets(String ipServer){
		try {
			socketServer = new Socket(ipServer, Protocol.SERVER_PORT);
			socketUDP = new DatagramSocket();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (SocketException e){
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public int getUDPPort(){
		return socketUDP.getPort();
	}
	public void addAd(int idAd, Ad ad){
		synchronized(this){
			ad.setId(idAd);
			ads.put(idAd, ad);
		}
	}
	public void removeAd(int id){
		synchronized(this){
			try{
				ads.remove(id);
			}catch(Exception e){}
		}
	}
	public Ad getAd(int id){
		synchronized(this){
			try{
				return ads.get(id);
			}catch(Exception e){
				return null;
			}
		}
	}
	public int[] getAdIds(){
		synchronized(this){
			int[] result = new int[ads.size()];
			int i=0;
			for(Map.Entry<Integer, Ad> entry:ads.entrySet()){
				result[i++] = entry.getKey();
			}
			return result;
		}
	}
	public synchronized void addTransactionToConclude(Transaction transaction){
		transactionsToConclude.add(transaction);
		notifyAll();
	}
	public synchronized boolean acceptTransaction(Transaction transaction){
		if(!transactionsToConclude.contains(transaction))
			return false;
		ClientObservable.getInstance().getMessage(
				new SendClientObservable(
							transaction.getUser().getIp(),
							transaction.getUser().getPortUdp(),
							"AD"+Protocol.MESSAGE_SEPARATION+"ACCEPT"+Protocol.MESSAGE_SEPARATION+"ID "+transaction.getAd()+Protocol.MESSAGE_SEPARATION
						));
		try {
			dos.writeUTF("AD"+Protocol.MESSAGE_SEPARATION+"DEL"+Protocol.MESSAGE_SEPARATION+"ID "+transaction.getAd()+Protocol.MESSAGE_SEPARATION);
			dos.flush();
		} catch (IOException e) {}
		try{
			transactionsToConclude.remove(transaction);
		}catch(Exception e){}
		notifyAll();
		return true;
	}
	public synchronized boolean declineTransaction(Transaction transaction){
		if(!transactionsToConclude.contains(transaction))
			return false;
		ClientObservable.getInstance().getMessage(
				new SendClientObservable(
							transaction.getUser().getIp(),
							transaction.getUser().getPortUdp(),
							"AD"+Protocol.MESSAGE_SEPARATION+"REFUSE"+Protocol.MESSAGE_SEPARATION+"ID "+transaction.getAd()+Protocol.MESSAGE_SEPARATION
						));
		try{
			transactionsToConclude.remove(transaction);
		}catch(Exception e){}
		notifyAll();
		return true;
	}
	public synchronized Transaction[] getTransactionsToConclude(){
		Object[] o = transactionsToConclude.toArray();
		Transaction[] t = new Transaction[o.length];
		for(int l=0; l<o.length; l++)
			if(o[l] instanceof Transaction)
				t[l] = (Transaction) o[l];
		return t;
	}
	public int sizeTransactionsToConclude(){
		return transactionsToConclude.size();
	}
	public synchronized void addTransactionAsking(int id) {
		Ad ad = this.getAd(id);
		try {
			if((ad.getIp().equals(InetAddress.getLocalHost().getHostAddress().toString()))
				&& (ad.getPort() == socketUDP.getLocalPort())){
				System.out.println("Transaction not authorized");
				return;
			}
			Transaction t = new Transaction(
					new User(ad.getIp(), 0, ad.getPort()),
					id);
			addClientTab(t);
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		transactionsAsking.add(id);
		ClientObservable.getInstance().getMessage(
				new SendClientObservable(
							ad.getIp(),
							ad.getPort(),
							"AD"+Protocol.MESSAGE_SEPARATION+"QUERY"+Protocol.MESSAGE_SEPARATION+"ID "+ad.getId()+Protocol.MESSAGE_SEPARATION
						));
	}
	public synchronized void acceptedTransaction(int id) {
		try{
			transactionsAsking.remove((Object)id);
		}catch(Exception e){}
		notifyAll();
	}
	public synchronized void declinedTransaction(int id) {
		try{
			transactionsAsking.remove((Object)id);
		}catch(Exception e){}
		notifyAll();
	}
	public synchronized Integer[] getTransactionsAsking(){
		Object[] o = transactionsAsking.toArray();
		Integer[] i = new Integer[o.length];
		for(int l=0; l<o.length; l++)
			if(o[l] instanceof Integer)
				i[l] = (int) o[l];
		return i;
	}
	public int sizeTransactionsAsking(){
		return transactionsAsking.size();
	}
	public synchronized void addClientTab(Transaction transaction){
		if(messagesClient.containsKey(transaction))
			return;
		ClientMessages m = new ClientMessages(transaction);
		messagesClient.put(transaction, m);
		tabbedClientMessages.add("", m);
	}
	public void start(){
		if(socketServer == null)
			return;
		try{
			scanner = new Scanner(System.in);
			pw = new PrintWriter(socketServer.getOutputStream());
			
			// Start threads
			threadServer = new ThreadServerToClient(socketServer);
			threadServer.start();
			threadUDP = new ThreadClientToClient(socketUDP);
			threadUDP.start();
			
			// Id message
			dos = new DataOutputStream(socketServer.getOutputStream());
			dos.writeUTF("USR"+Protocol.MESSAGE_SEPARATION+"PORT "+socketUDP.getLocalPort()+Protocol.MESSAGE_SEPARATION);
			dos.flush();
			
			doActions();
			
			pw.close();
			socketServer.close();
			scanner.close();
			System.exit(0);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	public void doActions(){
		try{
			int id = 0;
			Ad ad;
			String msg;
			mainLoop:
			while(true){
				System.out.print("> ");
				switch(scanner.nextLine()){
				case ActionOnClient.HELP:
				case "?":
					displayHelp();
					break;
				case ActionOnClient.EXIT:
					dos.writeUTF("DISCONNECT"+Protocol.MESSAGE_SEPARATION);
					dos.flush();
					break mainLoop;
				case ActionOnClient.LIST_ADS:
					displayAds();
					break;
				case ActionOnClient.ADD_AD:
					System.out.print("Write your ad:");
					msg = scanner.nextLine();
					dos.writeUTF("AD"+Protocol.MESSAGE_SEPARATION+"ADD"+Protocol.MESSAGE_SEPARATION+"MSG "+msg+Protocol.MESSAGE_SEPARATION);
					dos.flush();
					break;
				case ActionOnClient.REMOVE_AD:
					System.out.print("Write the id:");
					msg = scanner.nextLine();
					dos.writeUTF("AD"+Protocol.MESSAGE_SEPARATION+"DEL"+Protocol.MESSAGE_SEPARATION+"ID "+msg+Protocol.MESSAGE_SEPARATION);
					dos.flush();
					break;
				case ActionOnClient.ADDRESS:
					System.out.println("["+InetAddress.getLocalHost().getHostAddress()+":"+socketUDP.getLocalPort()+"]");
					break;
				case ActionOnClient.LIST_TRANSACTIONS_ASKING:
					Integer[] ids = getTransactionsAsking();
					for(Integer i:ids){
						System.out.println("["+i+"]"+getAd(i).getMessage());
					}
					break;
				case ActionOnClient.LIST_TRANSACTIONS_TO_CONCLUDE:
					Transaction[] tss = getTransactionsToConclude();
					for(Transaction t:tss){
						System.out.println("["+t.getAd()+"]"+getAd(t.getAd()).getMessage());
					}
					break;
				case ActionOnClient.SEND_MESSAGE_TO_CLIENT:
					System.out.print("Write the id:");
					try{
						id = Integer.parseInt(scanner.nextLine());
					}catch(Exception e){
						System.out.println("Id is an integer");
						break;
					}
					ad = getAd(id);
					if(ad == null){
						System.out.println("Id not exists");
						break;
					}	
					System.out.print("Write the message:");
					msg = scanner.nextLine();
					ClientObservable.getInstance().getMessage(
							new SendClientObservable(
										ad.getIp(),
										ad.getPort(),
										"AD"+Protocol.MESSAGE_SEPARATION+"ID "+id+Protocol.MESSAGE_SEPARATION+"MSG "+msg+Protocol.MESSAGE_SEPARATION
									));
					Transaction t = new Transaction(
							new User(ad.getIp(), 0, ad.getPort()),
							id);
					addClientTab(t);
					ClientObservable.getInstance().getMessage(
							new MessageClientObservable(
										id,
										InetAddress.getLocalHost().getHostAddress(),
										getUDPPort(),
										msg
									));
					break;
				case ActionOnClient.ASK_TRANSACTION:
					System.out.print("Write the id:");
					try{
						id = Integer.parseInt(scanner.nextLine());
					}catch(Exception e){
						System.out.println("Id is an integer");
						break;
					}
					if(getAd(id) == null){
						System.out.println("Id not exists");
						break;
					}	
					addTransactionAsking(id);
					break;
				case ActionOnClient.ACCEPT_TRANSACTION:
					Transaction[] ts = getTransactionsToConclude();
					for(int i=0; i<ts.length; i++){
						System.out.println("["+i+"]\t"+" "+ts[i]);
					}
					System.out.print("Choose an identifier:");
					try{
						id = Integer.parseInt(scanner.nextLine());
						if(id<0 || ts.length<=id)
							break;
						acceptTransaction(ts[id]);
					}catch(Exception e){
						System.out.println("Identifier is an integer");
						break;
					}
					acceptTransaction(ts[id]);
					break;
				case ActionOnClient.DECLINE_TRANSACTION:
					Transaction[] ts2 = getTransactionsToConclude();
					for(int i=0; i<ts2.length; i++){
						System.out.println("["+i+"]\t"+" "+ts2[i]);
					}
					System.out.print("Choose an identifier");
					try{
						id = Integer.parseInt(scanner.nextLine());
						if(id<0 || ts2.length<=id)
							break;
						declineTransaction(ts2[id]);
					}catch(Exception e){
						System.out.println("Identifier is an integer");
						break;
					}
					declineTransaction(ts2[id]);
					break;
				default:
					System.out.println("Error action, see \"help\"");
				}
			}
		}catch(Exception e){}
	}
	public void displayAds(){
		int[] adIds = this.getAdIds();
		for(int i=0; i<adIds.length; i++){
			try {
				Ad ad = this.getAd(adIds[i]);
				if((ad.getIp().equals(InetAddress.getLocalHost().getHostAddress().toString()))
						&& (ad.getPort() == socketUDP.getLocalPort()))
					System.out.print("*");
				System.out.print("["+ad.getId()+"]");
				System.out.println("\t"+ad.getMessage());
			} catch (UnknownHostException e) {}
		}
	}
	public void displayHelp(){
		System.out.println("Action on server");
		System.out.println("\t"+ActionOnClient.EXIT+"\texit the application");
		System.out.println("\t"+ActionOnClient.LIST_ADS+"\tlist ads");
		System.out.println("\t"+ActionOnClient.ADD_AD+"\tadd ad");
		System.out.println("\t"+ActionOnClient.REMOVE_AD+"\tremove an ad");
		System.out.println("Action with oneself");
		System.out.println("\t"+ActionOnClient.ADDRESS+"\tdisplay address and port");
		System.out.println("\t"+ActionOnClient.LIST_TRANSACTIONS_ASKING+"\ttransactions wanted");
		System.out.println("\t"+ActionOnClient.LIST_TRANSACTIONS_TO_CONCLUDE+"\ttransactions to conclude");
		System.out.println("Action with client");
		System.out.println("\t"+ActionOnClient.SEND_MESSAGE_TO_CLIENT+"\tsend a message to an other client");
		System.out.println("\t"+ActionOnClient.ASK_TRANSACTION+"\task to conclude transaction");
		System.out.println("\t"+ActionOnClient.ACCEPT_TRANSACTION+"\taccept to conclude a transaction");
		System.out.println("\t"+ActionOnClient.DECLINE_TRANSACTION+"\trefuse to conclude transaction");
	}
}
