package user;

public class User {
	private String ip;
	private int port;
	private int portUdp;
	/**
	 * Create an user
	 * @param ip String
	 * @param port int
	 * @param portUdp int
	 */
	public User(String ip, int port, int portUdp){
		setIp(ip);
		setPort(port);
		setPortUdp(portUdp);
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public int getPort() {
		return port;
	}
	public void setPort(int port) {
		this.port = port;
	}
	public int getPortUdp() {
		return portUdp;
	}
	public void setPortUdp(int portUdp) {
		this.portUdp = portUdp;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ip == null) ? 0 : ip.hashCode());
		result = prime * result + port;
		result = prime * result + portUdp;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (ip == null) {
			if (other.ip != null)
				return false;
		} else if (!ip.equals(other.ip))
			return false;
		if (port != other.port)
			return false;
		if (portUdp != other.portUdp)
			return false;
		return true;
	}
}
