package ad;

public class Ad {
	private int id;
	private String ip;
	private int port;
	private String message;
	/**
	 * Create an ad
	 * @param id int
	 * @param ip String
	 * @param port int
	 * @param message String
	 */
	public Ad(int id, String ip, int port, String message){
		setId(id);
		setIp(ip);
		setPort(port);
		setMessage(message);
	}
	/**
	 * Create an ad
	 * @param ip
	 * @param port
	 * @param message
	 */
	public Ad(String ip, int port, String message){
		setIp(ip);
		setPort(port);
		setMessage(message);
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public int getPort() {
		return port;
	}
	public void setPort(int port) {
		this.port = port;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Ad other = (Ad) obj;
		if (id != other.id)
			return false;
		if (ip == null) {
			if (other.ip != null)
				return false;
		} else if (!ip.equals(other.ip))
			return false;
		if (message == null) {
			if (other.message != null)
				return false;
		} else if (!message.equals(other.message))
			return false;
		if (port != other.port)
			return false;
		return true;
	}
}
