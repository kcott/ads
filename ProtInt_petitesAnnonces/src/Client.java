import client.ClientInstance;

public class Client {
	public static void main(String args[]){
		if(args.length != 1){
			System.err.println("Wait ip of server");
			System.exit(1);
		}
		ClientInstance client = ClientInstance.getInstance();
		client.setSockets(args[0]);
		client.start();
		/*GUIClient client = new GUIClient(args[0]);
		client.setSize(100, 200);
		client.setVisible(true);
		/*while(client.isActive()){
			client.close();
		}*/
	}
}
