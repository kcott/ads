import server.ServerInstance;

public class Server {
	/**
	 * Main of the server
	 * @param args String[] args in command line
	 */
	public static void main(String args[]){
		ServerInstance server = ServerInstance.getInstance();
		server.start();
	}
}
